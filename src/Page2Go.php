<?php
/**
 * Page2go library (03.2024)
 **/
namespace Parangon\Page2go;

use Exception;
use Parangon\Page2go\Templating\PageModels;

class Page2Go
{
    /** @var mixed */
    protected $templating;

    /** @var object|array */
    protected $data;

    /** @var array */
    protected array $defaultOptions = [
        'max_scope'     => 10,
        'navigate'      => true,
        'spacers'       => false,
        'actions_link'  => '?limit=#limit#&offset=#offset#',
        'active_class'  => 'active',
        'templates_dir' => null,
        'templates_ext' => '.html',
        'selectBgColor' => '#5E2961',
        'selectColor'   => '#FFFFFF'
    ];

    /*** @throws Exception */
    public function __construct($data = [], array $options = [], $templating = PageModels::PAGE2GO_LINK)
    {
        $this->setData($data)->setOptions($options)->setTemplating($templating);
    }

    /**
     * @param $templating
     * @return $this
     */
    private function setTemplating($templating): self
    {
        $this->templating = $templating;
        return $this;
    }

    /**
     * @param object|array $data
     * @return $this
     * @throws Exception
     */
    public function setData($data): self
    {
        if(is_object($data)) $data = (array)$data;

        if(is_array($data) && !empty($data)) {
            $required = ['limit', 'offset', 'total'];
            foreach ($required as $key) {
                if(!isset($data[$key])) throw new Exception("missing key : $key", 400);
                if(!is_numeric($data[$key])) throw new Exception("invalid key : $key, expected INT got ".strtoupper(gettype($data[$key])), 400);
                $data[$key] = intval($data[$key]);
            }
        } else {
            throw new Exception("invalid data", 400);
        }

        $this->data = $data;
        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options = []): self
    {
        $this->defaultOptions = array_merge($this->defaultOptions, $options);
        return $this;
    }

    /**
     * @return array
     */
    private function setTemplates(): array
    {
        $baseRoot  = dirname(__DIR__, 1) . "/templates/{$this->templating::dir()}/";
        $customDir = $this->defaultOptions['templates_dir'];
        $customExt = $this->defaultOptions['templates_ext'];

        foreach($this->templating::templates() as $index => $value)
        {
            $custom   = preg_replace("/^@\//", $customDir ?? "", str_replace('.html', $customExt, $value));
            $template = preg_replace("/^@\//", $baseRoot, $value);

            if(file_exists($file = $custom) || file_exists($file = $template)) {
                $templates[$index] = file_get_contents($file);
            }
        }

        return $templates ?? [];
    }

    /**
     * @deprecated replace with render
     * @param bool $withCSS
     * @param bool $withJS
     * @return string
     */
    public function load(bool $withCSS = true, bool $withJS = true): string
    {
        return (new $this->templating($this->setTemplates(), $this->data, $this->defaultOptions))->build($withCSS, $withJS);
    }

    /**
     * @param bool $withCSS
     * @param bool $withJS
     * @return string
     */
    public function render(bool $withCSS = true, bool $withJS = true): string
    {
        return (new $this->templating($this->setTemplates(), $this->data, $this->defaultOptions))->build($withCSS, $withJS);
    }
}