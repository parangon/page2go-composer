<?php

namespace Parangon\Page2go\Templating;

abstract class PageModels
{
    CONST PAGE2GO_LINK   = PageLink::class;
    CONST PAGE2GO_FILTER = PageFilter::class;
    CONST PAGE2GO_ARROW  = PageArrow::class;
}