<?php

namespace Parangon\Page2go\Templating;

interface PageInterface
{
    static public function dir();
    static public function templates();
    public function build(bool $withCSS, bool $withJS): string;
    public function replace(string $search, string $replace = ""): self;
    public function style(): string;
    public function script(): string;
}