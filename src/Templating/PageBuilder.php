<?php

namespace Parangon\Page2go\Templating;

trait PageBuilder
{
    protected array $templates;
    protected array $data;
    protected array $options;
    protected string $render;

    public function __construct(array $templates, array $data, array $options)
    {
        $this->templates = $templates;
        $this->data      = $data;
        $this->options   = $options;
        $this->render    = $templates['base'] ?? "";
    }

    /**
     * loop and replace #VARS# by method VARS() or templates[VARS]
     * @param bool $withCSS
     * @param bool $withJS
     * @return string
     */
    public function build(bool $withCSS, bool $withJS): string
    {
        if($this->data['limit'] >= $this->data['total']) {
            return "";
        }

        do {
            $i = preg_match("/#(.+?)#/", $this->render, $matches);

            if($i && method_exists(self::class, $matches[1])) {
                $this->replace($matches[0], self::{$matches[1]}());
            } else if($i && isset($this->templates[$matches[1]])) {
                $this->replace($matches[0], $this->templates[$matches[1]]);
            } else if($i){
                $this->replace($matches[0]);
            }
        } while ($i);

        if($withCSS) {
            $this->render = $this->style() . $this->render;
        }

        if($withJS) {
            $this->render = $this->render . $this->script();
        }

        return $this->render;
    }

    /**
     * replace $search by $replace in $this->render
     * @param string $search
     * @param string $replace
     * @return $this
     */
    public function replace(string $search, string $replace = ""): self
    {
        $this->render = str_replace($search, $replace, $this->render);
        return $this;
    }

    /**
     * replace with custom if necessary
     * @return string
     */
    public function style(): string
    {
        $style = str_replace('#selectBgColor#', $this->options['selectBgColor'], $this->templates['style'] ?? "");
        $style = str_replace('#selectColor#', $this->options['selectColor'], $style);
        return $style;
    }

    /**
     * replace with custom if necessary
     * @return string
     */
    public function script(): string
    {
        $script = str_replace('#link#', $this->options['actions_link'], $this->templates['script'] ?? "");
        $script = str_replace('#limit#', $this->data['limit'], $script);
        return $script;
    }
}