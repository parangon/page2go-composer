<?php

namespace Parangon\Page2go\Templating;

/**
 * @Documentation button to load next content
 * SEO friendly
 */
class PageArrow implements PageInterface
{
    use PageBuilder;
    
    static public function dir(): string
    {
        return "arrow";
    }
    
    static public function templates(): array
    {
        return [
            'base'  => '@/base.html',
            'prev'  => '@/prev-arrow.html',
            'next'  => '@/next-arrow.html',
            'space' => '@/space.html',
            'style' => '@/style.html'
        ];
    }

    private function arrows(): string
    {
        $pages  = $this->arrow('prev', $this->data['offset'] - $this->data['limit']);

        if($this->options['spacers']) {
            $pages .= $this->templates['space'];
        }

        $pages .= $this->arrow('next', $this->data['offset'] + $this->data['limit']);

        return $pages;
    }

    private function arrow(string $rel, int $i): string
    {
        $arrow = str_replace("#class#", $rel, $this->templates[$rel]);

        if(($rel === 'prev' && $i >= 0) || ($rel === 'next' && $i <= $this->data['total'])) {
            $link = str_replace(["#limit#", "#offset#"], [$this->data['limit'], $i], $this->options['actions_link']);
        } else {
            $link = "";
            $rel  = "nofollow";
        }

        $arrow = str_replace("#link#", $link, $arrow);
        $arrow = str_replace("#rel#", $rel, $arrow);

        return $arrow;
    }
}