<?php

namespace Parangon\Page2go\Templating;

/**
 * @Documentation html select with pages as options
 *  SEO friendly only with options['navigate'] == true
 */
class PageFilter implements PageInterface
{
    use PageBuilder;
    
    static public function dir(): string
    {
        return "filter";
    }

    static public function templates()
    {
        return [
            'base'   => '@/base.html',
            'pages'  => '@/pages.html',
            'option' => '@/option.html',
            'number' => '@/number.html',
            'arrow'  => '@/arrow.html',
            'style'  => '@/style.html',
            'script' => '@/script.html'
        ];
    }

    private function pages(): string
    {
        $pages = "";

        if($this->options['navigate']) {
            $pages .= $this->arrow('prev', $this->data['offset'] - $this->data['limit']);
        }

        $pages .= $this->templates['pages'];

        if($this->options['navigate']) {
            $pages .= $this->arrow('next', $this->data['offset'] + $this->data['limit']);
        }

        return $pages;
    }

    private function options(): string
    {
        $options  = "";

        for($offStart = 0; $offStart < $this->data['total']; $offStart += $this->data['limit']) {
            $options .= $this->option($offStart);
        }

        return $options;
    }

    private function option(int $i): string
    {
        $option = str_replace("#selected#", ($i === $this->data['offset'] ? 'selected' : ''), $this->templates['option']);
        $option = str_replace("#value#", $i, $option);

        $number = str_replace("#number#", floor($i / $this->data['limit']) + 1, $this->templates['number']);
        $option = str_replace("#number#", $number, $option);

        return $option;
    }

    private function arrow(string $rel, int $i): string
    {
        $arrow = str_replace("#class#", $rel, $this->templates['arrow']);

        if(($rel === 'prev' && $i >= 0) || ($rel === 'next' && $i <= $this->data['total'])) {
            $link = str_replace(["#limit#", "#offset#"], [$this->data['limit'], $i], $this->options['actions_link']);
        } else {
            $link = "";
            $rel  = "nofollow";
        }

        $arrow = str_replace("#link#", $link, $arrow);
        $arrow = str_replace("#rel#", $rel, $arrow);

        return $arrow;
    }
}