<?php

namespace Parangon\Page2go\Templating;

/**
 * @Documentation li list of pages
 * SEO friendly
 */
class PageLink implements PageInterface
{
    use PageBuilder;

    static public function dir(): string
    {
        return "link";
    }

    static public function templates()
    {
        return [
            'base'   => '@/base.html',
            'page'   => '@/page.html',
            'number' => '@/number.html',
            'space'  => '@/space.html',
            'arrow'  => '@/arrow.html',
            'style'  => '@/style.html'
        ];
    }

    private function pages(): string
    {
        $pages    = "";
        $offStart = 0;

        $total    = $this->data['total'];
        $limit    = $this->data['limit'];
        $offset   = $this->data['offset'];
        $maxScope = $this->options['max_scope'];
        $offLast  = (ceil($total / $limit) - 1) * $limit;

        if(is_int($maxScope) && $maxScope < 5) {
            $maxScope = 3;
            $this->options['spacers']  = false;
            $this->options['navigate'] = true;
        } else if(is_int($maxScope) && $maxScope * $limit > $total) {
            $this->options['spacers']  = false;
        }
        
        if($this->options['navigate']) {
            $pages .= $this->arrow('prev', $offset - $limit);
        }

        if(is_int($maxScope)) {
            $pages .= $this->page(0);

            $scope    = floor(($maxScope - 2) / 2);
            $offStart = max($limit, $offset - $scope * $limit);
            $total    = min($offLast, $offStart + ($maxScope - 2) * $limit);

            if($this->options['spacers'] && $offStart - $limit > 0) {
                $pages .= '#space#';
            }

            if(($diff = $maxScope - (($total - $offStart) / $limit) - 2) > 0) {
                $offStart = max($limit, $offStart - $diff * $limit);
            }
        }

        for($offStart; $offStart < $total; $offStart += $limit) {
            $pages .= $this->page($offStart);
        }

        if(is_int($maxScope)) {
            if($this->options['spacers'] && $offStart + $limit < $offLast) {
                $pages .= '#space#';
            }

            $pages .= $this->page($offLast);
        }

        if($this->options['navigate']) {
            $pages .= $this->arrow('next', $offset + $limit);
        }

        return $pages;
    }

    private function page(int $i): string
    {
        $page = str_replace("#class#", ($i === $this->data['offset'] ? $this->options['active_class'] : ''), $this->templates['page']);

        $link = str_replace(["#limit#", "#offset#"], [$this->data['limit'], $i], $this->options['actions_link']);
        $page = str_replace("#link#", $link, $page);

        $number = str_replace("#number#", floor($i / $this->data['limit']) + 1, $this->templates['number']);
        $page = str_replace("#number#", $number, $page);

        return $page;
    }

    private function arrow(string $rel, int $i): string
    {
        $arrow = str_replace("#class#", $rel, $this->templates['arrow']);

        if(($rel === 'prev' && $i >= 0) || ($rel === 'next' && $i <= $this->data['total'])) {
            $link = str_replace(["#limit#", "#offset#"], [$this->data['limit'], $i], $this->options['actions_link']);
        } else {
            $link = "";
            $rel  = "nofollow";
        }

        $arrow = str_replace("#link#", $link, $arrow);
        $arrow = str_replace("#rel#", $rel, $arrow);

        return $arrow;
    }
}