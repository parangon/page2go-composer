### parangon/feed2go

* Page2go library (03.2024)


### INSTALL
```
composer.json
```
```
{
    "require": {
            "parangon/page2go": "1.*"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/parangon/page2go-composer.git"
        }
    ]
}
```

###
### Playground
```
require_once "./vendor/autoload.php";
use Parangon\Page2go\Page2Go;
use Parangon\Page2go\Templating\Templates;

/*** pagination information */
$data = ['limit' => $_GET['limit'] ?? 5, 'offset' => $_GET['offset'] ?? 0, 'total' => 30];

/*** options */
$options = [
    // max number of pages
    'max_scope'     => 10,
    // add arrow around links or filter
    'navigate'      => true,
    // add space between first/last page when max scope is reached
    'spacers'       => false,
    // action link in links or filter or arrow
    'actions_link'  => '?limit=#limit#&offset=#offset#',
    // where to find your custom templates (ex: __DIR__ . '/templates/')
    'templates_dir' => null,
    // custom templates extension (ex: .html.twig)
    'templates_ext' => '.html',
    // custom li.active background color 
    'selectBgColor' => '#5E2961',
    // custom li.active color 
    'selectColor'   => '#FFFFFF'
];

/*** init */
$page2go = new Page2Go($data, $options, Templates::PAGE2GO_LINK);

/*** somewhere in your html */ 
<div id="pagination"><?= $page2go->load(); ?></div>
```

### Templates
* use Parangon\Page2go\Templating\Templates::CONST to load desired model
* see parangon/page2go/templates/{Model} to copy template files with custom
#### Example
````
/** Add file /my-templates/space.html.twig in your project */
<li> { custom spacer here } </li>
````
````
/*** Models */ 
// Ahref links with page number (SEO friendly)
$templating = Templates::PAGE2GO_LINK;

// Select pages as filter (NOT SEO friendly)
$templating = Templates::PAGE2GO_FILTER; 

// prev. next. arrow only (SEO friendly)
$templating = Templates::PAGE2GO_ARROW;

/*** Custom file */
$options = ['spacers' => true, 'templates_dir' => '/my-templates/', 'templates_ext' => '.html.twig'];
$page2go = new Page2Go($data, $options, Templates::PAGE2GO_ARROW);
````
<br>
contact : dev@parangon-creations.com